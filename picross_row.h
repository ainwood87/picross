#ifndef PICROSS_ROW_H
#define PICROSS_ROW_H

#include "puzzle_clue.h"
#include <stdbool.h>

/*Contains the information of which blocks are filled or not
 *implementation can change
 */
typedef struct picross_row_s * picross_row_t;

/*
 * initialize the picross_row_t
 * 
 * returns a picross_row_t, memory is always allocated
 * num_cols is the number of colums the row spans
 * clue is the puzzle_clue associated with this row in the puzzle
 *
 * clue is required as a parameter, regardless of how picross_row_t is 
 * implemented, because the clue at least provides information on how many
 * blocks there will be in the row, which could be useful.
 *
 * After initialization, the picross_row_t has taken whatever information 
 * from clue that it needs, so no other function requires the clue to be passed
 *
 */
picross_row_t init_picross_row(int num_cols, puzzle_clue_t clue);

/*
 * destructor 
 */
int destroy_picross_row(picross_row_t row);

/* Accessors */

/*
 * returns true if the ith square in the row is filled
 */
bool row_square_is_filled(picross_row_t row, int i);

/* Mutators */

/*
 * Set the state of the picross row to the inital state
 * That is with all blocks as far to the left as possible
 */
void initial_state(picross_row_t row);

/*
 * Changes the state of the row to the next logical state that is
 * consistent with the clues provided at init.
 *
 * returns true on success
 * false if called on last state
 */
bool iterate_state(picross_row_t row);

#endif
