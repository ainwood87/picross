#ifndef PICROSS_DEBUG_H
#define PICROSS_DEBUG_H
#include <stdio.h>

//#define DEBUG

#define UNUSED(x) ((void) x)

#ifdef DEBUG
#define picross_debug(...) printf(__VA_ARGS__)
#else
#define picross_debug(...)
#endif

#endif
