#include "picross_row.h"
#include "puzzle_clue.h"
#include "picross_debug.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define EOK (0)

static struct picross_row_s {
    int * first_cols;
    int num_blocks;
    int num_cols;
    puzzle_clue_t clue; //copy of clue
} picross_row_s;

/*
 * returns true if the ith block of row can move to the right
 */
static bool is_movable(picross_row_t row, int i)
{
    if (row->num_blocks - 1 == i) {
        return row->first_cols[i] + get_hint(row->clue, i) < row->num_cols;
    } else {
        return row->first_cols[i] + get_hint(row->clue, i) < row->first_cols[i + 1] - 1;
    }
}

picross_row_t init_picross_row(int num_cols, puzzle_clue_t clue)
{
    int num_hints, first_hint;

    picross_row_t row = malloc(sizeof(picross_row_s));
    if (NULL == row)
        goto fail0;

    if (0 > (num_hints = clue_num_hints(clue))) {
        printf("failed to get num_hints\n");
        goto fail0;
    }
    if (0 == num_hints) {
        row->num_blocks = 0;
    } else {
        first_hint = get_hint(clue, 0);
        row->num_blocks = (0 == first_hint) ? 0 : num_hints;
    }
    picross_debug("num_blocks for row is %d\n", row->num_blocks);
    row->first_cols = malloc(num_hints * sizeof(int));
    if (NULL == row->first_cols)
        goto fail1;
    
    if (NULL == (row->clue = init_puzzle_clue_copy(clue))) {
        printf("failed to copy clue\n");
        goto fail2;
    }
    row->num_cols = num_cols;
    return row;

fail2:
    free (row->first_cols);
fail1:
    free (row);
fail0:
    printf("failed to allocate picross_row_t\n");
    return NULL;
}

int destroy_picross_row(picross_row_t row)
{
    free (row->first_cols);
    free (row);
    return EOK;
}

void initial_state(picross_row_t row)
{
    int i, hint;
    picross_debug("initial_state num_blocks %d\n", row->num_blocks);
    if (row->num_blocks == 0)
        return;

    row->first_cols[0] = 0;
    for (i = 1; i < row->num_blocks; ++i) {
        hint = get_hint(row->clue, i - 1);
        row->first_cols[i] = row->first_cols[i - 1] + hint + 1;
    }
}

bool iterate_state(picross_row_t row)
{
    int i, j, hint;
    for (i = row->num_blocks - 1; i >= 0; --i) {
        if (is_movable(row, i))
            break;
    }
    if (i < 0)
        return false;

    ++row->first_cols[i];

    //reset blocks to the right
    for (j = i + 1; j < row->num_blocks; ++j) {
        hint = get_hint(row->clue, j - 1);
        row->first_cols[j] = row->first_cols[j - 1] + hint + 1;
    }
    return true;
}

bool row_square_is_filled(picross_row_t row, int i)
{
    //find the first block starting on or to the left of i
    int j, start, length;
    if (0 == row->num_blocks) { //no blocks on this row
        return false;
    }

    for (j = row->num_blocks - 1; j >= 0; --j) {
        if (i >= (start = row->first_cols[j])) {
            length = get_hint(row->clue, j);
            return (i >= start && i < start + length);
        }
    }
    return false; // all blocks to the right
}

