all: picross

picross: main.o picross_puzzle.o puzzle_clue.o picross_row.o
	gcc main.o picross_puzzle.o puzzle_clue.o picross_row.o -o picross

main.o: main.c
	gcc -c main.c

picross_puzzle.o: picross_puzzle.c
	gcc -c picross_puzzle.c

puzzle_clue.o: puzzle_clue.c
	gcc -c puzzle_clue.c

picross_row.o: picross_row.c
	gcc -c picross_row.c

clean:
	rm -rf *o  picross
