#ifndef PUZZLE_CLUE_H
#define PUZZLE_CLUE_H
/*
 * puzzle_clue.h
 *
 * Definition of the puzzle_clue data structure.
 * This data structure contains a sequence of integer values (hints), which
 * represent the size of blocks on a row/column in a picross puzzle
 *
 * An interface to this data structure is provided
 */

/*
 * The puzzle_clue_t data structure. Its implementation is hidden
 */
typedef struct puzzle_clue_s * puzzle_clue_t;

/* Constructors */

/*
 * initializes the puzzle clue
 * memory is always allocated
 */
puzzle_clue_t init_puzzle_clue(int * hints, int num_hints);

/*
 * returns a deep copy of src
 * returns null if fail
 */
puzzle_clue_t init_puzzle_clue_copy(puzzle_clue_t src);

/*
 * Destructor
 */
int destroy_puzzle_clue(puzzle_clue_t clue);

/* Accessors */

/*
 * returns the number of hints contained in this clue
 * returns a negative value on failure
 */
int clue_num_hints(puzzle_clue_t clue);

/*
 * returns the index'th hint
 * negative on error
 */
int get_hint(puzzle_clue_t clue, int index);

#endif
