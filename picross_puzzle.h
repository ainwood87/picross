#ifndef PICROSS_PUZZLE_H
#define PICROSS_PUZZLE_H

#include "puzzle_clue.h"
#include "picross_row.h"

#define EOK (0)

typedef struct picross_puzzle_s * picross_puzzle_t;

/*
 * initialize the picross puzzle, memory is always allocated
 *
 * num_rows, num_cols: the dimensions of the puzzle,
 * and arrays of clues for both the rows and columns
 *
 * returns EOK on success
 */
picross_puzzle_t init_picross_puzzle(int num_rows, int num_cols, puzzle_clue_t * row_clues, puzzle_clue_t * col_clues);

/* clears the memory for the puzzle */
void destroy_puzzle(picross_puzzle_t puzzle);

/*
 * solve_puzzle
 * 
 * Solves the picross puzzle specified by the input parameters, and if
 * successful stores the solution at puzzle
 * Uses a depth-first search with backtracking algorithm to solve the picross
 *
 * returns true if successful
 * returns false if no solution
 */
bool solve_puzzle(picross_puzzle_t puzzle);

/*
 * prints the current partial solution of the puzzle
 */
void print_solution(picross_puzzle_t puzzle);
#endif
