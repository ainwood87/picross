/*
 * main.c
 *
 * parses an input picross puzzle from stdin and feeds it to the picross solver
 */
#include <stdio.h>
#include <stdlib.h>
#include "picross_puzzle.h"
#include "puzzle_clue.h"
#include "picross_debug.h"

static int read_hints(int ** input_hints, size_t max_hints)
{
    char c;
    int i = 0;
    int val = 0;
    int digit;
    int num_hints = 0;
    int * hints = *input_hints;
    //will realloc later
    hints = malloc(max_hints * sizeof(int));
    for (;;) {
        c = getchar();
        switch (c) {
        case EOF:
            //fallthrough
        case '\n': /* finished reading the line */
            hints[i] = val; 
            num_hints = i + 1;
            *input_hints = realloc(hints, num_hints * sizeof(int));
            if (NULL == *input_hints) {
                free (hints); //last chance to free hints
                printf("realloc error\n");
                return -1;
            }
            return num_hints;
        case ' ':
            //finished parsing this hint
            hints[i] = val; 
            /* reset values */
            val = 0;
            ++i;
            break;
        default:
            digit = atoi(&c); 
            val = val * 10 + digit;
            break;
        }
    }
    return num_hints;
}

int main(int argc, void * argv[])
{
    /* Expected input is as follows:
     *
     * The first line on stdin contains two integers separated by one space, the
     * number of rows, R, and the number of columns, C.
     * 
     * The next R lines contain the hints for the rows, which are space
     * separated integers representing the size of blocks on the row
     * The next C lines contain the hints for the columns in the same fashion
     */
    int R, C;
    int i, j;

    //get dimensions
    scanf("%d %d\n", &R, &C);
    picross_debug("got dimensions %d %d\n", R, C);
    //arrays of pointers, since main, the client, must use the ADT interface to 
    //create puzzle_clue_t
    puzzle_clue_t * row_clues = malloc (R * sizeof (puzzle_clue_t));
    if (NULL == row_clues) {
        printf("malloc fail\n");
        goto fail0;
    }
    puzzle_clue_t * col_clues = malloc (C * sizeof (puzzle_clue_t));
    if (NULL == col_clues) {
        printf("malloc fail\n");
        goto fail1;
    }

    //Read in the hints

    //Array of arrays, one array per row. Each array of ints will be 
    //allocated by read_hints
    int ** row_hints = malloc(R * sizeof(int*));
    if (NULL == row_hints) {
        printf("malloc fail\n");
        goto fail2;
    }

    picross_debug("read row hints\n");
    //At max (1 + C) / 2 blocks
    for (i = 0; i < R; ++i) {
        int num_hints = read_hints(&row_hints[i], (1 + C) / 2);
        if (num_hints < 0)
            goto fail3;

        row_clues[i] = init_puzzle_clue(row_hints[i], num_hints);
        //already finished with row_hints[i], a copy was made in init_puzzle_clue
        free (row_hints[i]);
    }
    free (row_hints);

    picross_debug("read col hints\n");
    int ** col_hints = malloc(C * sizeof(int*));
    if (NULL == col_hints) {
        printf("malloc fail\n");
        goto fail3;
    }
    for (j = 0; j < C; ++j) {
        int num_hints = read_hints(&col_hints[j], (1 + R) / 2);
        if (num_hints < 0)
            goto fail4;

        col_clues[j] = init_puzzle_clue(col_hints[j], num_hints);
        free (col_hints[j]);
    }
    free (col_hints);

#ifdef DEBUG
    printf("Print out hints read from parser\n");
    //print out hints
    for (i = 0; i < R; ++i) {
        for (j = 0; j < clue_num_hints(row_clues[i]); ++j) {
            int hint = get_hint(row_clues[i], j);
            printf("%d ", hint);
        }
        printf("\n");
    }
    for (i = 0; i < C; ++i) {
        for (j = 0; j < clue_num_hints(col_clues[i]); ++j) {
            int hint = get_hint(col_clues[i], j);
            printf("%d ", hint);
        }
        printf("\n");
    }
#endif
    
    //initialize the puzzle so it can be solved
    picross_puzzle_t solution = init_picross_puzzle(R, C, row_clues, col_clues);
    //we are now done with the clues
    for (i = 0; i < R; ++i) {
        destroy_puzzle_clue(row_clues[i]);
    }
    free (row_clues);

    for (j = 0; j < C; ++j) {
        destroy_puzzle_clue(col_clues[j]);
    }
    free (col_clues);

    if (NULL == solution) {
        printf("failed to init picross_puzzle\n");
        return -1;
    }

    if (solve_puzzle(solution)) {
        printf("solved\n");
        print_solution(solution);
    }
    else
        printf("Puzzle does not have a solution\n");

    //cleanup puzzle's memory
    destroy_puzzle(solution);
    return 0;

fail4:
    if (j == C) {
        for (j = j - 1; j > 0; --j) {
            destroy_puzzle_clue(col_clues[j]);
        }
    } else {
        for (j = j - 1; j > 0; --j) {
            free (col_hints[j]);
            destroy_puzzle_clue(col_clues[j]);
        }
        free (col_hints);
    }
fail3:
    if (i == R) { //partial cleanup is already done
        for (i = i - 1; i > 0; --i) {
            destroy_puzzle_clue(row_clues[i]);
        }
    } else {
        for (i = i - 1; i > 0; --i) {
            free (row_hints[i]);
            destroy_puzzle_clue(row_clues[i]);
        }
        free (row_hints);
    }
fail2:
    free (col_clues);
fail1:
    free (row_clues);
fail0:
    return -1;
}
