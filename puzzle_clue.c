#include "puzzle_clue.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define EOK (0)

static struct puzzle_clue_s {
    int * hints;
    int num_hints;
} puzzle_clue_s;

puzzle_clue_t init_puzzle_clue(int * hints, int num_hints)
{
    puzzle_clue_t clue = malloc(sizeof(puzzle_clue_s));
    if (NULL == clue)
        goto fail0;

    clue->hints = malloc(num_hints * sizeof(int));
    if (NULL == clue->hints)
        goto fail1;

    memcpy(clue->hints, hints, num_hints * sizeof(int));
    clue->num_hints = num_hints;
    return clue;

fail1:
    free (clue);
fail0:
    printf("failed to allocate puzzle_clue_t\n");
    return NULL;
}

puzzle_clue_t init_puzzle_clue_copy(puzzle_clue_t src)
{
    return init_puzzle_clue(src->hints, src->num_hints);
}

int destroy_puzzle_clue(puzzle_clue_t clue)
{
    free (clue->hints);
    free (clue);
    return EOK;
}

int clue_num_hints(puzzle_clue_t clue)
{
    return clue->num_hints;
}

int get_hint(puzzle_clue_t clue, int index)
{
    if (index >= clue->num_hints)
        return -1;

    return clue->hints[index];
}

