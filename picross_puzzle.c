#include "picross_puzzle.h"
#include "picross_debug.h"
#include <stdlib.h>
#include <stdio.h>

struct picross_puzzle_s {
    int num_rows;
    int num_cols;
    puzzle_clue_t * col_clues;
    picross_row_t * rows;
} picross_puzzle_s;

/*
 * get the ith row
 */
static picross_row_t get_row(picross_puzzle_t puzzle, int i)
{
    return puzzle->rows[i];
}

static void print_row(picross_puzzle_t puzzle, int row_index)
{
#ifdef DEBUG
    picross_row_t row = get_row(puzzle, row_index);
    int i;
    for (i = 0; i < puzzle->num_cols; ++i) {
        printf("%c", row_square_is_filled(row, i) ? '#' : ' ');
    }
    printf("\n");
#else
    UNUSED(puzzle);
    UNUSED(row_index);
#endif
}

static bool square_is_filled(picross_puzzle_t puzzle, int row_num, int col_num)
{
    picross_row_t row = get_row(puzzle, row_num);
    bool is_filled = row_square_is_filled(row, col_num);
    return is_filled;
}

/*
 * return the length of the vertical block starting at row_num, col_num which occurs not after row_max
 */
static int get_block_size(picross_puzzle_t puzzle, int row_num, int col_num, int row_max)
{
    if (false == square_is_filled(puzzle, row_num, col_num))
        return -1;

    int len = 0;
    while (row_num <= row_max && square_is_filled(puzzle, row_num, col_num)) {
        ++len;
        ++row_num;
    }
    return len;
}

/*
 * assumes there is at least one vertical block in the column.
 * returns the index of the row that starts the first vertical block
 * in column col_num
 */
static int get_first_vertical_block(picross_puzzle_t puzzle, int col_num)
{
    int i;
    for (i = 0; i < puzzle->num_cols; ++i) {
        if (square_is_filled(puzzle, i, col_num))
            return i;
    }
}

/*
 * returns the index of the next vertical block, after the one started at row_num, col_num, 
 * but starting before or at row_max
 * If there are no more vertical blocks before row_max, the function will return a value > row_max
 */
static int get_next_vertical_block(picross_puzzle_t puzzle, int row_num, int col_num, int row_max)
{
    //find end of block
    while (row_num <= row_max && square_is_filled(puzzle, row_num, col_num))
        ++row_num;

    if (row_num > row_max)
        return row_num;

    //find beginning of next block
    while (row_num <= row_max && !square_is_filled(puzzle, row_num, col_num))
        ++row_num;

    //either we found the next block or we exceeded row_num. In either case
    //just return row_num
    return row_num;
}

static bool column_is_consistent(picross_puzzle_t puzzle, int col_num, int row_max)
{
    puzzle_clue_t col_clue = puzzle->col_clues[col_num];  
    if (0 == get_hint(col_clue, 0)) {
        //check that none are filled
        int r;
        for (r = 0; r <= row_max; ++r) {
            if (square_is_filled(puzzle, r, col_num)) {
                return false;
            }
        }
        return true;
    }
    /*
     * Find the last vertical block
     * find it's size and index, that's all you need
     */
    bool on_row_max = square_is_filled(puzzle, row_max, col_num);
    //find last block
    int row = row_max;
    while (row >= 0 && !square_is_filled(puzzle, row, col_num))
        --row;
    
    if (row == -1) //nothing filled, could still be valid
        return true;
    
    int block_size = 0;
    int last_block_row = row;
    //find the length of this block
    while (row >= 0 && square_is_filled(puzzle, row, col_num)) {
        --row;
        ++block_size;
    }
    //get the clue_index
    int clue_index = 0;
    while (row >= 0) {
        //find next block
        while (row >= 0 && !square_is_filled(puzzle, row, col_num))
            --row;

        if (row < 0) 
            break;
        
        ++clue_index;
        while (row >= 0 && square_is_filled(puzzle, row, col_num))
            --row;
    }

    int hint = get_hint(col_clue, clue_index);
    
    if (on_row_max) {
        if (block_size > hint)
            return false;
    } else {
        if (block_size < hint)
            return false;
    }
    int remaining = 0;
    int num_hints = clue_num_hints(col_clue);
    //check to see if we still have space to complete the column
    int i;
    for (i = clue_index + 1; i < num_hints; ++i)
        remaining += get_hint(col_clue, i) + 1; //+1 for padding

    if (!on_row_max)
        --remaining;
    //remaining is the min number of rows after row_max that we need to complete the row
    int rows_available = puzzle->num_rows - 1 - row_max;
    if (remaining > rows_available)
        return false;

    return true;
}

/*
 * Checks the current partial solution for consistency with the col_hints field.
 * Consistency with the row_hints field is assumed
 * Returns true if none of the column clues are contradicted by blocks on rows <= row_max
 */
static bool check_all_column_conditions(picross_puzzle_t puzzle, int row_max)
{
    int i;
    for (i = 0; i < puzzle->num_cols; ++i) {
        if (!column_is_consistent(puzzle, i, row_max))
            return false;
    }
    return true;
}

static bool state_is_valid(picross_puzzle_t puzzle, int row_index)
{
    return check_all_column_conditions(puzzle, row_index);
}

static bool get_first_valid_state(picross_puzzle_t puzzle, int row_index)
{
    picross_row_t row = get_row(puzzle, row_index);
    initial_state(row);
    while (!state_is_valid(puzzle, row_index)) {
        picross_debug("invalid state. Iterate\n");
        print_row(puzzle, row_index);
        if (!iterate_state(row)) {
            picross_debug("failed to iterate state\n");
            return false;
        }
    }
    return true;
}

static bool get_next_valid_state(picross_puzzle_t puzzle, int row_index)
{
    picross_row_t row = get_row(puzzle, row_index);
    do {
        if (!iterate_state(row))
            return false;
    } while (!state_is_valid(puzzle, row_index));
    return true;
}

picross_puzzle_t init_picross_puzzle(int num_rows, int num_cols, puzzle_clue_t * row_clues, puzzle_clue_t * col_clues)
{
    int i, j;

    picross_puzzle_t puzzle = malloc(sizeof(picross_puzzle_s));
    if (NULL == puzzle) {
        printf("failed to allocate picross_puzzle_t\n");
        goto fail0;
    }

    puzzle->col_clues = malloc(num_cols * sizeof(puzzle_clue_t));
    if (NULL == puzzle->col_clues) {
        printf("failed to allocate col_clues\n");
        goto fail1;
    }
    for (i = 0; i < num_cols; ++i) {
        puzzle->col_clues[i] = init_puzzle_clue_copy(col_clues[i]);
        if (NULL == puzzle->col_clues[i])
            goto fail2;
    }

    puzzle->rows = malloc(num_rows * sizeof(picross_row_t));
    if (NULL == puzzle->rows) {
        printf("failed to allocate picross_row_t array\n");
        goto fail2;
    }
    for (j = 0; j < num_rows; ++j) {
        puzzle->rows[j] = init_picross_row(num_cols, row_clues[j]);
        if (NULL == puzzle->rows[j]) {
           printf("failed to init row %d\n", j); 
           goto fail3;
        }
    }

    puzzle->num_rows = num_rows;
    puzzle->num_cols = num_cols;

    return puzzle;

fail3:
    --j;
    while (j > 0) {
        destroy_picross_row(puzzle->rows[j]);
        --j;
    }
    free (puzzle->rows);
fail2:
    --i;
    while (i > 0) {
        destroy_puzzle_clue(puzzle->col_clues[i]);
        --i;
    }
    free (puzzle->col_clues);
fail1:
    free (puzzle);
fail0:
    printf("failed to init picross puzzle\n");
    return NULL;
}

void destroy_puzzle(picross_puzzle_t puzzle)
{
    int i;
    if (puzzle->num_rows && puzzle->rows != NULL) {
        for (i = 0; i < puzzle->num_rows; ++i)
            destroy_picross_row(puzzle->rows[i]);
        free (puzzle->rows);
    }
    if (puzzle->num_cols && puzzle->col_clues != NULL) {
       for (i = 0; i < puzzle->num_cols; ++i) 
           destroy_puzzle_clue(puzzle->col_clues[i]);
       free (puzzle->col_clues);
    }
    free (puzzle);
}



bool solve_puzzle(picross_puzzle_t puzzle)
{
    int row_index = 0;
    if (!get_first_valid_state(puzzle, row_index)) {
        printf("first row has no valid state\n");
        return false;
    }
    picross_debug("first valid state:\n");
    print_row(puzzle, row_index);
    /*
     * preconditions
     * the row at row_index is in a valid state
     * The loop will break the next time a valid row is found,
     * which will either be the row at row_index - 1, or an earlier
     * row if backtracking was required
     * The loop will also break if no more valid rows are possible, which
     * either happens when the 0th row cannot be made valid, or if the loop
     * is entered with row-index == R-1
     */
    for (;;) {
        if (puzzle->num_rows - 1 == row_index) {
            return true;
        }

        //enter new row
        ++row_index;
        picross_debug("enter row %d\n", row_index);
        if (get_first_valid_state(puzzle, row_index)) {
            picross_debug("got first valid state\n");
            print_row(puzzle, row_index);
            continue;
        }

        //backtracking
        do {
            --row_index;
            picross_debug("backtrack to row %d\n", row_index);
            if (row_index < 0)
                return false;
            picross_debug("previous state:\n");
            print_row(puzzle, row_index);
        } while (!get_next_valid_state(puzzle, row_index));
        picross_debug("used state:\n");
        print_row(puzzle, row_index);
    }

    return false;
}

void print_solution(picross_puzzle_t puzzle)
{
    int i, j;
    for (i = 0; i < puzzle->num_rows; ++i) {
        for (j = 0; j < puzzle->num_cols; ++j) {
            printf("%c", square_is_filled(puzzle, i, j) ? '#' : '.');
        }
        printf("\n");
    }
}

